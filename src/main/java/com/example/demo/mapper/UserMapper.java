package com.example.demo.mapper;

import com.example.demo.model.User;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface UserMapper {

    @Select("select * from user where user_id = #{userId}")
    User getUserById(Integer id);

    @Results({
            @Result(property = "userId", column = "user_id"),
            @Result(property = "userName", column = "user_name"),
            @Result(property = "sex", column = "sex"),
            @Result(property = "createdTime", column = "created_time")
    })
    @Select("select * from user")
    List<User> getAllUsers();

    @Delete("delete from user where user_id = #{userId}")
    int deleteUser(User user);

    @Insert(" insert into user(user_id, user_name, sex,created_time)\n" +
            "        values(#{userId},#{userName}, #{sex}, #{createdTime})")
    int addUser(User user);

    @Update("update user set user_name = #{userName}, sex = #{sex}, created_time = #{createdTime} where user_id = #{userId}")
    int updateUser(User user);
}