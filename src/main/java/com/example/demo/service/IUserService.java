package com.example.demo.service;

import com.example.demo.model.User;

import java.util.List;

public interface IUserService {
    public List<User> getAllUsers() ;
    public int addUser(User user) ;
    public int deleteUser(User user);
    public int updateUser(User user);
    public User getUserById(Integer id);

}
